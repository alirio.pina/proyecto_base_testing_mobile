package com.test.stepdefinition;

import com.test.question.TheScreenShouldHas;
import com.test.setup.SetUp;
import com.test.task.ExampleTask;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.junit.Before;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class ExampleSD extends SetUp {

    @Before
    public void before(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("Start")
    public void start() {
        actor.can(BrowseTheWeb.with(hisMobileDevice));
    }

    @When("Middle")
    public void middle() {

        actor.attemptsTo(
                ExampleTask.exampleTask()
        );

    }
    @Then("End")
    public void end() {

        actor.should(
                seeThat(TheScreenShouldHas.exampleCounter(), containsString("2/3"))
        );

    }

}
