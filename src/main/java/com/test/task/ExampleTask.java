package com.test.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static com.test.ui.ExampleOverview.EXAMPLE_BTN;

public class ExampleTask implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(

        Click.on(EXAMPLE_BTN)

        );
    }

    public static ExampleTask exampleTask() {return new ExampleTask();}

}
