package com.test.question;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;


public class TheScreenShouldHas {

    public static final Target EXAMPLE_TARGET = Target
            .the("Example target")
            .located(By.id("com.digitalchemy.calculator.freedecimal:id/helpScreenCounter"));

    public static Question<String> exampleCounter(){
        return TheTarget.textOf(EXAMPLE_TARGET);
    }

    private TheScreenShouldHas() {
    }

}
